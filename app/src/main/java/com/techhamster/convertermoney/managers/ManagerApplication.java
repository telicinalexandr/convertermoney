package com.techhamster.convertermoney.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.widget.Toast;

import com.techhamster.convertermoney.R;
import com.techhamster.convertermoney.SharedKeys;

public class ManagerApplication extends android.app.Application {
    private ManagerShared sharedManager;

    private static ManagerApplication instance;

    public static ManagerApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (!getSharedManager().getValueBoolean(SharedKeys.IS_FIRT_RUN)) {
            getSharedManager().putKeyBoolean(SharedKeys.IS_FIRT_RUN, true);
            getSharedManager().putKeyString(SharedKeys.FROM, "USD");
            getSharedManager().putKeyString(SharedKeys.TO, "RUB");
        }
    }

    public ManagerShared getSharedManager() {
        if (sharedManager == null) {
            setSharedManager(new ManagerShared(getApplicationContext()));
        }
        return sharedManager;
    }

    public void setSharedManager(ManagerShared sharedManager) {
        this.sharedManager = sharedManager;
    }

    /**
     * Проверка на наличие интернета
     *
     * @param isVisibleError - показывать ли ошибку что интернета нету
     * @return boolean - true - есть интернет
     * false - нет интернета
     */
    public boolean isConnectToInternet(boolean isVisibleError) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isOnline = netInfo != null && netInfo.isConnectedOrConnecting();
        if (isOnline) {
            return true;
        } else {
            if (isVisibleError) {
                showToast(R.string.not_connection_network);
            }
            return false;
        }
    }

    /**
     * Показ всплывающего окна в контексте прилояжения
     *
     * @param resString - id строки из ресурсов
     */
    public void showToast(int resString) {
        Toast toast = Toast.makeText(getApplicationContext(), resString, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 100);
        toast.show();
    }

    public void showToastShort(int resString) {
        Toast toast = Toast.makeText(getApplicationContext(), resString, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 100);
        toast.show();
    }

    public void showToastLong(int resString) {
        Toast toast = Toast.makeText(getApplicationContext(), resString, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 100);
        toast.show();
    }

    public void showToast(String string) {
        Toast toast = Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 100);
        toast.show();
    }
}
