package com.techhamster.convertermoney.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techhamster.convertermoney.model.ISO;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ManagerShared {
    private Context context;
    private SharedPreferences settings;

    public ManagerShared(Context c) {
        context = c;
        setSettings(context.getSharedPreferences("mode", context.MODE_PRIVATE));
    }

    public void putKeyString(String key, String value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void putKeyBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getValueString(String key) {
        return getSettings().getString(key, "");
    }

    public boolean getValueBoolean(String key) {
        return getSettings().getBoolean(key, false);
    }

    public SharedPreferences getSettings() {
        if (settings == null) {
            settings = context.getSharedPreferences("mode", context.MODE_PRIVATE);
        }
        return settings;
    }

    public void setSettings(SharedPreferences settings) {
        this.settings = settings;
    }


    public void putKeyInteger(String key, int value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getValueInteger(String key) {
        return getSettings().getInt(key, 0);
    }

    public List<ISO> getList(String key){
        Gson gson = new Gson();
        Type type = new TypeToken<List<ISO>>() {}.getType();
        return gson.fromJson(getValueString(key), type);
    }

    public void saveList(String key, ArrayList<ISO> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        putKeyString(key, json);
    }
}