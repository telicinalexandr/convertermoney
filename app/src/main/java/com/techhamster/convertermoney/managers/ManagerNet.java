/*
 * TechHamster (c) 2018.
 */

package com.techhamster.convertermoney.managers;

import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by gly8 on 21.03.17.
 */

public class ManagerNet {
    /**
     * Чтение тела ответа
     *
     * @param body - тело запроса ошибочный или нормальный
     * @return считанная строка
     **/
    public String readResultFromResponse(ResponseBody body) {
        return new String(getBytesFromInputStream(body.byteStream()));
    }

    public byte[] getBytesFromInputStream(InputStream inputStream) {
        try {
            String line;
            StringBuilder sb = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString().getBytes();
        } catch (OutOfMemoryError | IOException e) {
            return null;
        }
    }
}