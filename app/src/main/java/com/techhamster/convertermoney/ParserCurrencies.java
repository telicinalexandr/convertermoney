package com.techhamster.convertermoney;

import android.os.AsyncTask;

import com.techhamster.convertermoney.model.ISO;
import com.techhamster.convertermoney.network.IAsyncParse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParserCurrencies extends AsyncTask<String, Integer, ArrayList<ISO>> {
    
    public IAsyncParse delegate;

    public ParserCurrencies(IAsyncParse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected ArrayList<ISO> doInBackground(String... params) {
        ArrayList<ISO> list = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(params[0]);
            JSONArray arr = obj.getJSONArray("currencies");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject o = arr.getJSONObject(i);
                ISO iso = new ISO();
                iso.setIso(o.getString("iso"));
                iso.setName(o.getString("currency_name"));
                iso.setIsObsolete(o.getBoolean("is_obsolete"));
                list.add(iso);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return list;
    }

    @Override
    protected void onPostExecute(ArrayList<ISO> urlLists) {
        super.onPostExecute(urlLists);
        if(delegate != null){
            delegate.CompleteParse(urlLists);
        }
    }
}