package com.techhamster.convertermoney;

public class SharedKeys {
    public static final String FROM = "FROM";
    public static final String TO = "TO";
    public static final String IS_FIRT_RUN = "IS_FIRT_RUN";
    public static final String ISO_LIST = "ISO_LIST";
    public static final String COUNT_RESPONSE_SERVER = "COUNT_RESPONSE_SERVER";
}
