package com.techhamster.convertermoney.model;

import com.techhamster.convertermoney.spinner.InterfaceSpinnerItem;

// Валюта доступная для конвертации
public class ISO implements InterfaceSpinnerItem {

    private String iso;
    private String name ;
    private boolean isObsolete;

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return iso;
    }

    @Override
    public void setValue(String value) {

    }

    public boolean getIsObsolete() {
        return isObsolete;
    }

    public void setIsObsolete(boolean isObsolete) {
        this.isObsolete = isObsolete;
    }
}
