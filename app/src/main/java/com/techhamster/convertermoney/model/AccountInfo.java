package com.techhamster.convertermoney.model;

// Информация об аккаунте с которого осуществляется запрос
public class AccountInfo {
    String id;
    String packageName;
    String serviceStartTimestamp;
    String serviceEndTimestamp;
    int packageLimit;
    int packageLimitRemaining;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getServiceStartTimestamp() {
        return serviceStartTimestamp;
    }

    public void setServiceStartTimestamp(String serviceStartTimestamp) {
        this.serviceStartTimestamp = serviceStartTimestamp;
    }

    public String getServiceEndTimestamp() {
        return serviceEndTimestamp;
    }

    public void setServiceEndTimestamp(String serviceEndTimestamp) {
        this.serviceEndTimestamp = serviceEndTimestamp;
    }

    public int getPackageLimit() {
        return packageLimit;
    }

    public void setPackageLimit(int packageLimit) {
        this.packageLimit = packageLimit;
    }

    public int getPackageLimitRemaining() {
        return packageLimitRemaining;
    }

    public void setPackageLimitRemaining(int packageLimitRemaining) {
        this.packageLimitRemaining = packageLimitRemaining;
    }
}