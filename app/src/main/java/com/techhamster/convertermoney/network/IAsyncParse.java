package com.techhamster.convertermoney.network;

import com.techhamster.convertermoney.model.ISO;

import java.util.ArrayList;

// Окончание асинхронного парсинга
public interface IAsyncParse {
    void CompleteParse(ArrayList<ISO> list);
}
