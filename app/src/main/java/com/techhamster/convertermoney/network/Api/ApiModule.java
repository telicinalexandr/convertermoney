package com.techhamster.convertermoney.network.Api;

import com.techhamster.convertermoney.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class ApiModule {
    public ApiModule() {
    }

    public ApiInterface getApiInterfaceByUrl(final String url) {
        return createBuilder(url).build().create(ApiInterface.class);
    }

    private Retrofit.Builder createBuilder(final String url) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://xecdapi.xe.com")
                .client(getClient());
        return builder;
    }

    public OkHttpClient getClient() {
        // Логирование запросов
//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        OkHttpClient client = clientBuilder
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
//                .addInterceptor(loggingInterceptor)
                .addInterceptor(new BasicAuthInterceptor(BuildConfig.ACCOUNT_ID, BuildConfig.API_KEY))
                .build();
        return client;
    }

    public class BasicAuthInterceptor implements Interceptor {

        private String credentials;

        public BasicAuthInterceptor(String user, String password) {
            this.credentials = Credentials.basic(user, password);
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request authenticatedRequest = request.newBuilder()
                    .header("Authorization", credentials).build();
            return chain.proceed(authenticatedRequest);
        }

    }
}