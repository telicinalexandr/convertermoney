package com.techhamster.convertermoney.network.Api;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by zettig on 30.11.2016.
 */

@SuppressWarnings("ALL")
public interface ApiInterface {
    /**
     * Получение токена ссервака в случае если он будет какими нибудь пидарами скомпреметирован
     */
    @GET()
    Call<ResponseBody> getDatafromUrl(@Url String url);
}
