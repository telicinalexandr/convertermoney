package com.techhamster.convertermoney;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.techhamster.convertermoney.managers.ManagerApplication;
import com.techhamster.convertermoney.managers.ManagerNet;
import com.techhamster.convertermoney.model.AccountInfo;
import com.techhamster.convertermoney.model.ISO;
import com.techhamster.convertermoney.network.Api.ApiModule;
import com.techhamster.convertermoney.network.IAsyncParse;
import com.techhamster.convertermoney.spinner.AdapterSpinner;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements IAsyncParse {

    private String TAG = "MainActivity";

    private AdapterSpinner adapterFrom, adapterTo;

    private Spinner spinnerFrom, spinnerTo;

    private TextView titleInfoCountResponse;

    private EditText editTextAmmount, editTextResultConvert;

    private ProgressBar progressBar;

    private Button btnConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerTo = findViewById(R.id.spinner_to_currencies);
        spinnerFrom = findViewById(R.id.spinner_from_currencies);
        titleInfoCountResponse = findViewById(R.id.title_info_count_response);
        progressBar = findViewById(R.id.progress_bar);
        editTextAmmount = findViewById(R.id.edit_text_amount);
        editTextResultConvert = findViewById(R.id.edit_text_result_convert);
        btnConvert = findViewById(R.id.btn_convert);

        editTextAmmount.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    responseConvert();
                    return true;
                }
                return false;
            }
        });

        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            showProgressBar();
            responseGetInfoAccount();
            responseGetAllCurrencies();
        } else {
            initCurrenciesAdapter(new ArrayList<>(ManagerApplication.getInstance().getSharedManager().getList(SharedKeys.ISO_LIST)));
            updateTitleCountServiceResponseServer();
        }
    }

    // Парсинг доступных валют
    private void parseResultCurrencies(String result) {
        ParserCurrencies parserCurrencies = new ParserCurrencies(this);
        parserCurrencies.execute(result);
    }

    //====================================
    // Response methods
    //====================================

    // Запрос на конвертацию валюты из сохраненных данных
    private void responseConvert() {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            String ammountStr = editTextAmmount.getText().toString();
            if (TextUtils.isEmpty(ammountStr)) {
                ManagerApplication.getInstance().showToast(R.string.please_write_amount);
                return;
            }

            double ammount = Double.parseDouble(ammountStr);
            if (ammount == 0) {
                ManagerApplication.getInstance().showToast(R.string.error_valid_summ);
                return;
            }
            String from = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.FROM);
            String to = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.TO);

            showProgressBar();
            ApiModule apiModule = new ApiModule();
            apiModule.getApiInterfaceByUrl("xecdapi.xe.com")
                    .getDatafromUrl("https://xecdapi.xe.com/v1/convert_from.json/?from=" + from + "&to=" + to + "&amount=" + ammountStr)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            showConvertResult(new ManagerNet().readResultFromResponse(response.body()));
                            responseGetInfoAccount();
                            hideProgressBar();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            hideProgressBar();
                            ManagerApplication.getInstance().showToast(R.string.error_response);
                        }
                    });
        }
    }

    // Получение списка оступных валют
    private void responseGetAllCurrencies() {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ApiModule apiModule = new ApiModule();
            apiModule.getApiInterfaceByUrl("xecdapi.xe.com")
                    .getDatafromUrl("https://xecdapi.xe.com/v1/currencies.json/?obsolete=true")
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            hideProgressBar();
                            parseResultCurrencies(new ManagerNet().readResultFromResponse(response.body()));
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            ManagerApplication.getInstance().showToast(R.string.error_response);
                            hideProgressBar();
                        }
                    });
        }
    }

    // Запрос на получение информации профиля и количества оставшихся запросов к серверу.
    private void responseGetInfoAccount() {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ApiModule apiModule = new ApiModule();
            apiModule.getApiInterfaceByUrl("xecdapi.xe.com").getDatafromUrl("https://xecdapi.xe.com/v1/account_info/").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        JSONObject obj = new JSONObject(new ManagerNet().readResultFromResponse(response.body()));
                        AccountInfo accountInfo = new AccountInfo();
                        accountInfo.setId(obj.getString("id"));
                        accountInfo.setPackageName(obj.getString("package"));
                        accountInfo.setServiceStartTimestamp(obj.getString("service_start_timestamp"));
                        accountInfo.setServiceEndTimestamp(obj.getString("service_end_timestamp"));
                        accountInfo.setPackageLimit(obj.getInt("package_limit"));
                        accountInfo.setPackageLimitRemaining(obj.getInt("package_limit_remaining"));
                        ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.COUNT_RESPONSE_SERVER, accountInfo.getPackageLimitRemaining());
                        updateTitleCountServiceResponseServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    ManagerApplication.getInstance().showToast(R.string.error_response);
                }
            });
        }
    }

    // Парсинг доступных валют завершен
    @Override
    public void CompleteParse(ArrayList<ISO> urlLists) {
        Collections.sort(urlLists, new Comparator<ISO>() {
            @Override
            public int compare(ISO o1, ISO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        ManagerApplication.getInstance().getSharedManager().saveList(SharedKeys.ISO_LIST, urlLists);
        initCurrenciesAdapter(urlLists);
    }
    //====================================
    // UI methods
    //====================================

    // отображение результата конвертации валюты
    private void showConvertResult(String resutlt) {
        if (TextUtils.isEmpty(resutlt)) {
            return;
        }

        try {
            JSONObject obj = new JSONObject(resutlt);
            JSONArray toArray = obj.getJSONArray("to");
            JSONObject to = toArray.getJSONObject(0);
            editTextResultConvert.setText(String.format("%.4f", to.getDouble("mid")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Обновление количество оставшихся попыток для запроса к серверу
    private void updateTitleCountServiceResponseServer() {
        int countResponseToServer = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.COUNT_RESPONSE_SERVER);
        String text = getString(R.string.count_service_response) + " " + countResponseToServer;
        Spannable wordtoSpan = new SpannableString(text);
        StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        wordtoSpan.setSpan(bss,
                getString(R.string.count_service_response).length(), text.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        titleInfoCountResponse.setText(wordtoSpan);
    }

    private void initCurrenciesAdapter(ArrayList<ISO> isoList) {
        if (isoList == null)
            return;

        adapterFrom = createSpinnerAdapter(spinnerFrom, isoList, new AdapterView.OnItemSelectedListener() {
            boolean isFirstSet = true;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isFirstSet) {
                    isFirstSet = false;
                } else {
                    ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.FROM, adapterFrom.getSelectedValue());
                    updateHintFrom();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        adapterTo = createSpinnerAdapter(spinnerTo, isoList, new AdapterView.OnItemSelectedListener() {
            boolean isFirstSet = true;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isFirstSet) {
                    isFirstSet = false;
                } else {
                    ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.TO, adapterTo.getSelectedValue());
                    updateHintTo();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinnerFrom.post(new Runnable() {
            @Override
            public void run() {
                updateHintFrom();
            }
        });

        spinnerTo.post(new Runnable() {
            @Override
            public void run() {
                updateHintTo();
            }
        });
    }

    private void updateHintFrom() {
        String from = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.FROM);
        adapterFrom.setSelectedPositionByValue(from);
        editTextAmmount.setHint(from);
    }

    private void updateHintTo() {
        String to = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.TO);
        adapterTo.setSelectedPositionByValue(to);
        editTextResultConvert.setHint(to);
    }

    private AdapterSpinner createSpinnerAdapter(Spinner spinner, ArrayList<ISO> data,
                                                AdapterView.OnItemSelectedListener adapterDelegate) {
        AdapterSpinner adapter = new AdapterSpinner(this, R.layout.item_spinner_check, data, spinner, adapterDelegate);
        spinner.setAdapter(adapter);
        return adapter;
    }

    public void showProgressBar() {
        btnConvert.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        btnConvert.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public void onClickResponseConvert(View view) {
        responseConvert();
    }
}