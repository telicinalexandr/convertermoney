package com.techhamster.convertermoney.spinner;

/**
 * Created by Sergey on 12.05.17.
 */

public class DefaultObject implements InterfaceSpinnerItem {
    private String name;
    private String value;

    public DefaultObject(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;

    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
