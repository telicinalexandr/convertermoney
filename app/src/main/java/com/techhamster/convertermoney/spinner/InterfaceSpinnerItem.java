package com.techhamster.convertermoney.spinner;

/**
 * Created by gly8 on 23.03.17.
 */

public interface InterfaceSpinnerItem {
    String getName();
    void setName(String name);
    String getValue();
    void setValue(String value);
}
