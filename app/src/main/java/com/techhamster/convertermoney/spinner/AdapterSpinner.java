package com.techhamster.convertermoney.spinner;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import com.techhamster.convertermoney.R;
import com.techhamster.convertermoney.model.ISO;

import java.util.ArrayList;

/**
 * Created by gly8 on 23.03.17.
 */

public class AdapterSpinner extends ArrayAdapter<ISO> {
    private ArrayList<ISO> data;
    private LayoutInflater inflater;
    private int resLayout;
    private int selectedPosition;
    private Spinner spinner;
    private AdapterView.OnItemSelectedListener delegate;

    //==========================================
    // Constructors
    //==========================================
    public AdapterSpinner(Context context, int resource, ArrayList<ISO> data, Spinner spinner) {
        super(context, resource, data);
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
        initSpinner(spinner);
    }

    public AdapterSpinner(Context context, int resource, ArrayList<ISO> data, Spinner spinner, AdapterView.OnItemSelectedListener delegate) {
        super(context, resource, data);
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
        this.delegate = delegate;
        initSpinner(spinner);
    }

    private void initSpinner(Spinner spinner) {
        if (spinner != null) {
            this.spinner = spinner;
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                boolean isInitCalling = true;

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!isInitCalling) {
                        setSelectedPosition(position);
                        notifyDataSetChanged();
                        if (delegate != null) {
                            delegate.onItemSelected(parent, view, position, id);
                        }
                    }
                    isInitCalling = false;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    //==========================================
    // View created
    //==========================================
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(parent, true, data.get(position), convertView, position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            return getCustomView(parent, false, data.get(position), convertView, position);
        } catch (IndexOutOfBoundsException e) {
            if (convertView == null || convertView != null) {
                return getCustomView(parent, false, data.get(0), convertView, position);
            }
        }
        return convertView;
    }

    private class ViewHolder {
        TextView title;
        ImageView imgCheck;
        ImageView imgArrow;

        public ViewHolder(View holder) {
            this.title = (TextView) holder.findViewById(R.id.title);
            this.imgCheck = (ImageView) holder.findViewById(R.id.img_check);
        }
    }

    public View getCustomView(ViewGroup parent, boolean isDropDown, ISO option, View convertView, int position) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resLayout, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        convertView.setPadding(0, convertView.getPaddingTop(), convertView.getPaddingRight(), convertView.getPaddingBottom());
        if (isDropDown) {
            if (selectedPosition == position) {
                holder.imgCheck.setAlpha(1f);
            } else {
                holder.imgCheck.setAlpha(0f);
            }
        } else {
            holder.imgCheck.setVisibility(View.GONE);
            if (holder.imgArrow != null) {
                holder.imgArrow.setVisibility(View.VISIBLE);
            }
        }


        holder.title.setText(option.getName()+" ("+option.getValue()+")");

        return convertView;
    }

    //==========================================
    // Help methods
    //==========================================
    public ISO getItemPosition(int position) {
        return data.get(position);
    }

    public int getItemCount() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    public void setSelectedPositionByValue(Object value) {
        for (int i = 0; i < data.size(); i++) {
            try {
                if (data.get(i).getValue().equals(value.toString())) {
                    setSelectedPosition(i);
                    spinner.setSelection(i);
                    break;
                }
            } catch (Exception e) {

            }
        }
    }

    public String getSelectedValue() {
        return getValueByPosition(selectedPosition);
    }

    public String getValueByPosition(int position) throws ArrayIndexOutOfBoundsException {
        return data.get(position).getValue();
    }

    public String getSelectedName() {
        return getNameByPosition(selectedPosition);
    }

    public String getNameByPosition(int position) throws ArrayIndexOutOfBoundsException {
        return data.get(position).getName();
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}